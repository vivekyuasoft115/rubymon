class User < ActiveRecord::Base
	has_many :monsters, dependent: :destroy
	has_many :teams, dependent: :destroy

	def self.from_omniauth(auth)
		user = {
			provider: auth[:provider],
			uid: auth[:uid],
			name: auth[:info][:name],
			oauth_token: auth[:credentials][:token],
			oauth_expires_at: Time.at(auth[:credentials][:expires_at])
		}
	 	User.create_with(user).find_or_create_by(user.slice(:provider, :uid))
	end
end
