class Team < ActiveRecord::Base
	belongs_to :user
	has_many :monsters, dependent: :destroy
	validates :name, presence: true

end
