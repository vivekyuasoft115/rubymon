class Monster < ActiveRecord::Base
	belongs_to :user
	belongs_to :team
	validates :name, :power, :monster_type, presence: true

	enum monster_type: [ :fire, :water, :earth, :electric, :wind ]

end

