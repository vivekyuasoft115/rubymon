class Api::V1::SessionsController < ApplicationController
  respond_to :json

  # POST /api/v1/auth/:provider/callback
	def create
  	user = User.from_omniauth(params[:auth])
    session[:user_id] = user.id
    render json: { status: :success, user: user.id }
  end

	# GET /api/v1/current_user
  def login_user
  	render json: { user: current_user }
  end
  
	# DELETE /api/v1/signout
  def destroy
    session[:user_id] = nil
    render json: { status: :ok }
  end


end
