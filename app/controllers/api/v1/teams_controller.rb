class Api::V1::TeamsController < ApplicationController
	before_action :authenticate_user
  before_action :set_team, only: [:show, :edit, :update, :destroy]
  before_action :get_teams, only: [:create]
  respond_to :json


  # GET /api/v1/team
  def index
  	teams = current_user.teams.all
  	render json: { status: :ok, teams: teams }
  end

  # GET /api/v1/team/:id
 	def show
 		render json: @team
  end

  # GET /api/v1/team/new
  def new
    team = team.new
    render json: team
  end

  # POST /api/v1/team
	def create
    team = current_user.teams.new(team_params)
    if team.save
 			render json: { status: :created, team: team }
 		else
 			render json: { status: :unprocessable_entity, errors: team.errors }
 		end
  end

  # PUT/PATCH /api/v1/team/:id
  def update
    if @team.update(team_params)
      render json: { status: :ok, team: @team }
    else
    	render json: { status: :unprocessable_entity, errors: team.errors }
    end
  end

  # DELETE /api/v1/team/:id
  def destroy
    @team.destroy 
    render json: { status: :success, head: :no_content }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = current_user.teams.find_by_id(params[:id])
      if @team.nil?
        render json: { status: :unprocessable_entity, errors: "could not found team with id #{params[:id]}" }
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(:id, :name)
    end

    def authenticate_user
      if !current_user
        render json: { status: :fail, errors: "Need to Login first" }
      end
    end

    def get_teams
      if current_user.teams.count > 2
        render json: { status: :fail, errors: "You Exeed your maximum limit of creating teams" }
      end
    end
end
