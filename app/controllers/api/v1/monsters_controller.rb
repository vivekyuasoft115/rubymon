class Api::V1::MonstersController < ApplicationController

  before_action :authenticate_user
  before_action :set_monster, only: [:show, :edit, :update, :destroy]
  before_action :get_team, only: [:create, :update]
  before_action :get_monsters, only: [:create]
  respond_to :json


  # GET /api/v1/monster
  def index
  	monsters = current_user.monsters.all	
    render json: { status: :ok, monsters: monsters }
  end

  # GET /api/v1/monster/:id
 	def show
 		render json: @monster
  end

  # GET /api/v1/monster/new
  def new
    monster = Monster.new
    render json: monster
  end

  # POST /api/v1/monster
	def create
    monster = current_user.monsters.new(monster_params)
    if monster.save
 			render json: { status: :created, monster: monster }
 		else
 			render json: { status: :unprocessable_entity, errors: monster.errors }
 		end
  end

  # PUT/PATCH /api/v1/monster/:id
  def update
    if @monster.update(monster_params)
      render json: { status: :ok, monster: @monster }
    else
    	render json: { status: :unprocessable_entity, errors: monster.errors }
    end
  end

  # DELETE /api/v1/monster/:id
  def destroy
    @monster.destroy 
    render json: { status: :success, head: :no_content }
  end
  
  # GET /api/v1/monsters/short_by/:type
  def short_by
    if params[:type] == "name"
      render json: { status: :ok, monsters: current_user.monsters.sort_by(&:name) }
    elsif  params[:type] == "power"
      render json: { status: :ok, monsters: current_user.monsters.sort_by(&:power) }
    elsif params[:type] == "weakness"
      render json: { status: :ok, monsters: current_user.monsters.order("monster_type") }
    else
      render json: { status: :fail, errors: "Wrong search type" }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_monster
      @monster = current_user.monsters.find_by_id(params[:id])
      if @monster.nil? 
        render json: { status: :unprocessable_entity, errors: "could not found monster with id #{params[:id]}" }
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def monster_params
      params.require(:monster).permit(:id, :name, :power, :monster_type, :team_id)
    end

    def authenticate_user
      if !current_user
        render json: { status: :fail, errors: "Need to Login first" }
      end
    end

    def get_monsters
      if current_user.monsters.count > 19
        render json: { status: :fail, errors: "You Exeed your maximum limit of creating monsters" }
      end
    end

    def get_team
      team = current_user.teams.find_by_id(params[:team_id])
      if team.nil?
        render json: { status: :fail, errors: :invalid_team }
      end
    end
end
