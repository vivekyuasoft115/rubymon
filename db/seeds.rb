# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#Creating User
user = User.create( provider: "facebook", uid: "12300015642359123", name: "Vivek Soni", oauth_token: "ADwWEKDx7cBAIWiXQ7XUg8w1Lp9eieA7jQKw3wz", oauth_expires_at: Time.at(1458024398) )

#Creating Team
team1 = user.teams.create( name: "Team 1")
team2 = user.teams.create( name: "Team 2")
team3 = user.teams.create( name: "Team 3")

#Creating Monster
user.monsters.create(team_id: team1.id, name: "Fogpaw",power: "Murmur",monster_type: "fire")
user.monsters.create(team_id: team1.id, name: "Mistghoul",power: "Cimeies",monster_type: "water")
user.monsters.create(team_id: team2.id, name: "Dustfigure",power: "Focalor",monster_type: "earth")
user.monsters.create(team_id: team2.id, name: "Smogmonster",power: "Gremory",monster_type: "electric")
user.monsters.create(team_id: team3.id, name: "The Empty Blob",power: "Asmoday",monster_type: "wind")
user.monsters.create(team_id: team3.id, name: "Rottingwings",power: "Balam",monster_type: "water")

puts "done........"